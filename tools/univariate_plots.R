# Plots of one variable ordered by that variable from least to greatest
# or in the case of the univariate plot where the user can select which variable
# to plot, the user can also choose the variable to order the genomes by.

# Update the plot data for the N50, genes and contigs univariate plots
output$N50plot <- reactive({
  scatterplot_output(id='N50plot'
    ,y_variable='N50'
    ,log_scale_y=input$N50plot_log_scale)
})

output$genes_1500bp_plot <- reactive({
  scatterplot_output(id='genes_1500bp_plot'
    ,y_variable='genes_gt_1500bp'
    ,log_scale_y=input$genes_1500bp_plot_log_scale)
})

output$contigs_500bp_plot <- reactive({
  scatterplot_output(id='contigs_500bp_plot'
    ,y_variable='contigs_gt_500bp'
    ,log_scale_y=input$contigs_500bp_plot_log_scale)
})

# Observe changes in the point opacity for the N50, genes and contigs univariate
# plots
observe({
  update_scatterplot_point_opacity(session, 'N50plot', input$N50plot_point_opacity)
})
observe({
  update_scatterplot_point_opacity(session, 'genes_1500bp_plot', input$genes_1500bp_plot_point_opacity)
})
observe({
  update_scatterplot_point_opacity(session, 'contigs_500bp_plot', input$contigs_500bp_plot_point_opacity)
})

# Update the text inputs for the N50, genes and contigs univariate plots based
# off user selection of points in the plots
observe({
  update_group_creation_textinputs('N50plot')
})

observe({
  update_group_creation_textinputs('genes_1500bp_plot')
})

observe({
  update_group_creation_textinputs('contigs_500bp_plot')
})

# If the user clicks the "Create user group" button in the plot selection dialog
# create a new category and group based off the selected genomes 
observe({
  create_user_group_from_scatterplot_selection('N50plot')
})

observe({
  create_user_group_from_scatterplot_selection('genes_1500bp_plot')
})

observe({
  create_user_group_from_scatterplot_selection('contigs_500bp_plot')
})



observe({
  update_scatterplot_popup_table_data('N50plot')
})
observe({
  update_scatterplot_popup_table_data('genes_1500bp_plot')
})
observe({
  update_scatterplot_popup_table_data('contigs_500bp_plot')
})
