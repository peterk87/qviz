


get_scatterplot_colours <- function(vals
  ,quantitative=TRUE
  ,selected_groups=NULL
  ,log_scale=TRUE)
{
  if (quantitative) #continuous numerical
  {
    if(log_scale)
    {
      vals <- log10(vals)
      vals[vals == -Inf] <- 1000
      vals[vals == 1000] <- min(vals)
    }
    
    min_val <- min(vals, na.rm=TRUE)
    max_val <- max(vals, na.rm=TRUE)
    # if the min value is the same as the max value then there is only one
    # value in the list of values for a quanititative colour scheme
    if(min_val == max_val)
    {
      group_colours <- 'black'
      group_labels <- selected_groups
      point_colours <- rep('black', length(vals))
      return(list(group_colours=group_colours
        ,group_labels=group_labels
        ,point_colours=point_colours)) 
    }
    group_colours <- rgb(colorRamp(brewer.pal(n=11, name='Spectral'))(0:10/10)/255)
    group_labels <- if(log_scale)
      {
        sprintf('%.1f',10^(((max_val-min_val)/10) * 0:10 + min_val))
      }
      else
      {
        sprintf('%.1f',((max_val-min_val)/10) * 0:10 + min_val)
      }
    is_NA <- is.na(vals)
    scaled_vals <- (vals - min_val)/(max_val-min_val)
    scaled_vals[is_NA] <- 0
    point_colours <- rgb(colorRamp(brewer.pal(n=11, name='Spectral'))(scaled_vals)/255)
    point_colours[is_NA] <- '#00000000'
  }
  else # qualitative/categorical
  {
    qual_colours <- qualitative_colours(length(selected_groups))
    point_colours <- vals
    point_colours[!(point_colours %in% selected_groups | is.na(point_colours))] <- '#00000000'
    count <- 1
    while (count <= length(selected_groups))
    {
      if (selected_groups[count] == NA_STR)
      {
        point_colours[is.na(point_colours)] <- qual_colours[count]
      }
      else
      {
        point_colours[point_colours == selected_groups[count]] <- qual_colours[count]
      }

      count <- count + 1
    }
    group_colours <- qual_colours[1:length(selected_groups)]
    group_labels <- selected_groups
    point_colours <- point_colours
  }
  list(group_colours=group_colours
    ,group_labels=group_labels
    ,point_colours=point_colours
  )
}


scatterplot_output <- function(id
  ,y_variable
  ,log_scale_y=FALSE
  ,order_by_variable=NULL
  ,x_variable=NULL
  ,log_scale_x=FALSE)
{
  df <- values$quast_df
  if(!is_valid_input(df)) return()

  names <- df$Assembly

  ys <- df[[y_variable]]
  
  if(log_scale_y)
  {
    ys <- log10(ys)
    ys[ys == -Inf] <- 0
    y_variable <- paste0('log10(',y_variable,')')
  }

  if(is.null(x_variable))
  {
    # if no x-variable supplied then rank by the Y-variable or another variable
    order_by <- if(is.null(order_by_variable))
      {
        ys
      }
      else
      {
        # if the order_by_variable is supplied then order by its values
        df[[order_by_variable]]
      }
    xs <- rank(order_by, ties.method='first')
    x_variable <- 'index'
  }
  else
  {
    xs <- df[[x_variable]]
    if(log_scale_x)
    {
      xs <- log10(xs)
      xs[xs == -Inf] <- 0
      x_variable <- paste0('log10(',x_variable,')')
    }
  }

  scatterplot_colours <- values$scatterplot_colours

  list(names=names
    ,xs=xs
    ,ys=ys
    ,groups=scatterplot_colours$group_labels
    ,group_colours=scatterplot_colours$group_colours
    ,selected_group=input$highlight_category_selection
    ,colours=scatterplot_colours$point_colours    
    ,x_label=x_variable
    ,y_label=y_variable
    ,reset_view_id=paste0(id, '_reset_view')
  )
}

update_group_creation_textinputs <- function(id)
{
  sel_genomes <- input[[id]]
  if(!is_valid_input(sel_genomes)) return()

  updateTextInput(session
    ,paste0(id, '_category_name')
    ,value=isolate({ifelse(input[[paste0(id, '_category_name')]] == ''
      ,'user_category_1'
      ,input[[paste0(id, '_category_name')]]
    )})
  )

  updateTextInput(session
    ,paste0(id, '_user_group_name')
    ,label=paste0('Group name for ', length(sel_genomes), ' selected genomes:')
    ,value='user_group_1'
  )
}

create_user_group_from_scatterplot_selection <- function(id)
{
  if(!is_valid_input(input[[paste0(id, '_create_user_group_field')]])) return()
  if(input[[paste0(id, '_create_user_group_field')]] == 0) return()
  isolate({
    df <- values$quast_df

    if(!(input[[paste0(id, '_category_name')]] %in% colnames(df)))
    {
      df[[input[[paste0(id, '_category_name')]]]] <- rep(input[[paste0(id, '_default_user_group')]], nrow(df))
    }

    df[df$Assembly %in% input[[id]], input[[paste0(id, '_category_name')]]] <- input[[paste0(id, '_user_group_name')]]

    values$quast_df <- df
    cols <- colnames(df)
    cols <- cols[!(cols %in% QUAST_HEADERS)]
    values$quast_df_melt <- melt(values$quast_df, id.vars=c('Assembly', cols))
    
    cols <- get_df_col_names(values$quast_df)
    cols <- cols[!(cols == 'Assembly')]
    # Update the category to highlight in the Scatterplot triggering a change of
    # the groups highlighted in the plot
    updateSelectInput(session
      ,'highlight_category_selection'
      ,choices=cols
      ,selected=input[[paste0(id, '_category_name')]]
    )
  })
}

get_metadata_subset_for_popup_table <- function(fields)
{
  md <- values$quast_df
  if(!is_valid_input(md))
   return()
  names <- md$Assembly
  if(!is_valid_input(fields))
    return()
  md <- md[, fields]
  list(names=names, md=md)
}

update_scatterplot_popup_table_data <- function(id)
{
  scatterplot_popup_table_fields <- input$scatterplot_popup_table_fields
  # There should be at least one field selected to display in the point popup 
  # table
  if(!is_valid_input(scatterplot_popup_table_fields))
    return()
  # The assembly name should always be present 
  scatterplot_popup_table_fields <- c('Assembly', scatterplot_popup_table_fields)
  data <- get_metadata_subset_for_popup_table(scatterplot_popup_table_fields)
  if(is.null(data))
    return()
  update_scatterplot_metadata(session, id, data$names, data$md)
}