
### Source Code

This is an open-source project and the source code is freely available under the GPLv2 license at https://bitbucket.org/peterk87/qviz

### Links of Interest

- <a href="http://bioinf.spbau.ru/quast" target="_blank">QUAST</a>: Quality Assessment Tool for Genome Assemblies
- <a href="http://quast.bioinf.spbau.ru/manual.html#sec3.1">QUAST assembly quality metrics descriptions</a>

### Issues?

Let us know what the issue is <a href="https://bitbucket.org/peterk87/qviz/issues?status=new&status=open" target="_blank">here</a>!

