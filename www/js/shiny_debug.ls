switch_to_tab = !-> 
  $ "\#tabs li a:contains(\'PCA\')" .trigger \click

switch_to_tab_2 = !->
  $ "\#tabs li a:contains(\'Bivariate Plot\')" .trigger \click

switch_to_tab_3 = !->
  $ "\#tabs li a:contains(\'N50 Plot\')" .trigger \click

debug_selectize = !->
  $ "\#highlight_category_selection option" .attr \value  \n_missing
  $ "\#highlight_category_selection" .trigger \change
  # $ "\#highlight_selected_category_as_numeric" .trigger \click

# setTimeout switch_to_tab, 1000
setTimeout switch_to_tab_2, 2000
# setTimeout switch_to_tab_3, 200
setTimeout debug_selectize, 3000