/*
 * Scatterplot
 *
 * Generate an interactive scatterplot using D3 from an object containing x, y
 *
 * Features:
 *   - multiple selection of points with shift key press
 *   - hover over points for a table showing point information
 *   - panning and zooming capabilities
 *   - automatic legend generation
 *
 * Dependencies:
 *   - D3 (version 3+)
 *
*/
export class Scatterplot
  /*
   * Shift key pressed? If so enable user selection of points with a D3 
   * selection brush.
   * @type {Boolean}
  */
  _shiftKey: false
  /*
   * Names of selected points
   * @type {Array}
  */
  _selection: []
  /*
   * Show the point info tooltip table or if already shown then keep the info
   * static in the tooltip table.
   * @type {Boolean}
  */
  _show_tt: false
  /*
   * Has the tooltip been moved/dragged by the user? If so don't change it's
   * position and assume that the user wants the tooltip in that position.
   * @type {Boolean}
  */
  _tt_dragged: false

  xs: []
  ys: []


  _xAxis: void
  _yAxis: void

  /*
   * @constructor
   *
   * The constructor only initializes the Scatterplot SVG and associated 
    elements. Data is brought into the plot using the update_data function.
   *
   * The point information tooltip is created and initialized.
   *
   * Set up global event handlers for keeping track of Shift key press and mouse move events. 
   * 
   * @param  {Element} @el div HTML element id
   * @param  {Integer} @width  SVG width
   * @param  {Integer} @height SVG height
   * @param  {Object} @margin  The left, right, top, and bottom int margins
  */
  (@el, 
    @width = 800, 
    @height = 600, 
    @margin = {top: 20, right: 20, bottom: 30, left: 60}) ->

    /*
     * Scatterplot div container HTML element ID
     * @type {String}
    */
    @_el_id := @el.id

    /*
     * SVG element width
     * @type {Integer}
    */
    @width := @width - @margin.left - @margin.right
    /*
     * SVG element height
     * @type {Integer}
    */
    @height := @height - @margin.top - @margin.top

    # init point tooltip
    @_tt := d3.select @el
      .append \div
      .attr  \class             \tooltip
      .style \display           \none
      .style \position          \absolute
      .style \background-color  \white
      .style \font-size         \10pt
      .style \text-shadow       \none
      .style \opacity           \0.9
      .style \border-radius     \10px
      .style \padding           \5px
    
    tt_move_btn = @_tt.append \h4
      .attr \class "btn btn-info tooltip-move"
      .style \cursor \move
      .append \i
      .attr \class "fa fa-arrows"
    tt_move_btn.text "\nMove"

    $ "\##{@_el_id} .tooltip" .draggable do
      handle: ".tooltip-move"
      start: !~>
        @_tt_dragged := true

    # add hide tooltip button to tooltip
    tt_close_btn = @_tt.append \h4
      .attr \class "btn btn-danger"
      .on \click !~> @hide_tooltip!
      .append \i
      .attr \class "fa fa-times-circle"
    tt_close_btn.text "\nClose"

    @_tt.append \br


    # init x and y linear scaling functions
    @_x := d3.scale.linear!.range [0, @width]
    @_y := d3.scale.linear!.range [@height, 0]

    # remove previous svg element
    d3.select @el .select \svg .remove!

    @_svg := d3.select @el
      .append \svg
      .attr \width @width + @margin.left + @margin.right
      .attr \height @height + @margin.top + @margin.bottom
    
    @_points_group := @_svg.append \g
      .attr \class "points-group"
      .attr \transform "translate(#{@margin.left},#{@margin.top})"

    @_brush := @_points_group.append \g 
      .datum ->
        selected: false
        previouslySelected: false
      .attr \class \brush
      .call do 
        d3.svg.brush!
          .x d3.scale.identity!.domain [0, @width]
          .y d3.scale.identity!.domain [0, @height]
          .on \brushstart !~> @brush_start_event!
          .on \brush !~> @brush_event!
          .on \brushend !~> @brush_end_event!
    
    /**
     * Format a numeric axis tick label using scientific notation if a number is 
     * above a threshold (default: 10000).
     * @param  {Numeric} d Axis tick label number
     * @return {String} Formatted axis tick label
    */
    format_axis_tick_label = (d, threshold = 10000) ->
      if d >= threshold or d <= -threshold
        f = d3.format \e 
      else
        f = d3.format \g 
      return f d

    @_xAxis := d3.svg.axis!
      .scale @_x 
      .orient \bottom
      .tickFormat -> format_axis_tick_label it
        
    @_yAxis := d3.svg.axis!
      .scale @_y 
      .orient \left
      .tickFormat -> format_axis_tick_label it
    # initialize X-axis
    @_points_group.append \g
      .attr \class "x axis"
      .attr \transform "translate(0,#{@height})"
      .call @_xAxis
      .append \text
      .attr \class \label
      .attr \x @width
      .attr \y -6
      .style \text-anchor \end
    # initialize Y-axis
    @_points_group.append \g
      .attr \class "y axis"
      .call @_yAxis
      .append \text
      .attr \class \label
      .attr \transform "rotate(-90)"
      .attr \y 6
      .attr \dy ".71em"
      .style \text-anchor \end

    @_rect := @_points_group.append \rect
      .attr \pointer-events \all
      .attr \width @width
      .attr \height @height
      .style \fill \none
      .style \cursor \move

    # set event handlers for Shift key press and mouse move events
    @_svg.on \click !~>
      d3.select window
        .on \keydown !~> @shiftKey_press_event!
        .on \keyup !~> @shiftKey_press_event!
        # .on \mousemove handle_mousemove_event

  /**
   * Update the point coordinate data coming into the plot as well as the axes 
   * labels. The point names are used as keys within the D3 data function. 
   * This function rescales and resets the view in the plot so that all points
   * are visible in the plot view box.
   * @param  {Object Array} data    Point data including x and y coordinates, 
   *                        name of point, colour, and other points bundled with
   *                        the point that have the same x and y coordinates.
   * @param  {String} x_label X-axis label
   * @param  {String} y_label Y-axis label
  */
  update_data: (data, x_label, y_label) !->
    @_data := data
    
    @_svg.select \.x.axis .select \.label 
      .text x_label
    @_svg.select \.y.axis .select \.label
      .text y_label


    @_points := @_points_group.selectAll \circle
      .data @_data, -> it.name

    @_points.enter!
      .append \circle
      .attr \class \point
      .style \fill-opacity 1
      .attr \r     -> 5 + it.other.length
      .attr \cx    ~> @_x it.x
      .attr \cy    ~> @_y it.y
      .style \fill -> it.c
      .attr \title -> it.name

    @_points.transition!
      .style \fill -> it.c
      .attr \r     -> 5 + it.other.length
      .attr \cx    ~> @_x it.x
      .attr \cy    ~> @_y it.y

    @_points.exit!
      .transition!
      .style \fill-opacity 0
      .attr \r 0
      .remove!

    @_points
      .on \mouseover (d) !~>
        @_points.classed \active (p) -> p is d
        @show_tooltip d if @_show_tt
      .on \click  !~>
        @_show_tt := not @_show_tt
        @show_tooltip it
      .on \mouseout  !~>
        @_points.classed \active false

  /**
   * Update PCA rotation vectors with new coordinates and labels.
   *
   * @param {String Array} rot_labels Rotation labels
   * @param {Float Array} rotx Rotation vector x coordinates
   * @param {Float Array} roty Rotation vector y coordinates
   * @param {String} category_select_id Select input id for changing to category
   * to highlight in the plot using colours. When a label is clicked, the 
   * selected category is changed. 
  */
  update_rotation_vectors: (rot_labels, rotx, roty, category_select_id=null) !->
    if category_select_id?
      @category_select_id := category_select_id
    @_rot_data := []
    for i from 0 til rot_labels.length
      @_rot_data.push do
        x: rotx[i]
        y: roty[i]
        label: rot_labels[i]
    
    @_x_rot := d3.scale.linear!.range [0, (d3.max @_data, (d) -> d.x)]
    @_y_rot := d3.scale.linear!.range [0, (d3.max @_data, (d) -> d.y)]
    @_x_rot.domain [0, 1] .nice!
    @_y_rot.domain [0, 1] .nice!

    @_rot_lines := @_points_group.selectAll \.rot-line
      .data @_rot_data, (d) -> d.label

    @_rot_lines.select \line
      .transition!
      .attr \x1 @_x 0
      .attr \y1 @_y 0
      .attr \x2 (d) ~> @_x @_x_rot d.x
      .attr \y2 (d) ~> @_y @_y_rot d.y

    @_rot_lines.select \text
      .attr \x (d) ~> @_x @_x_rot d.x
      .attr \y (d) ~> 
        y_offset = if d.y < 0 then 10 else 0
        y_offset + @_y @_y_rot d.y
      .attr \text-anchor (d) -> if d.x > 0 then \start else \end

    rot_line_enter = @_rot_lines.enter!.append \g
      .attr \class \rot-line

    rot_line_enter.append \line
      .attr \x1 @_x 0
      .attr \y1 @_y 0
      .attr \x2 (d) ~> @_x @_x_rot d.x
      .attr \y2 (d) ~> @_y @_y_rot d.y
      .style \stroke \black
      .style \stroke-width 2

    rot_line_enter.append \text
      .attr \x (d) ~> @_x @_x_rot d.x
      .attr \y (d) ~> 
        y_offset = if d.y < 0 then 10 else 0
        y_offset + @_y @_y_rot d.y
      .text (d) -> d.label
      .attr \text-anchor (d) -> if d.x > 0 then \start else \end
      .style \cursor \help
      .on \click (d) !~> 
        @click_rotation_label d

    @_rot_lines.exit!
      .transition!
      .remove!

  /**
   * Remove rotation vector lines from plot
  */
  remove_rotation_vectors: !->
    @_rot_lines.transition!.remove!

  /**
   * Update the point colours without resetting the view box and rescaling the
   * axes so that the user zoom/pan view is preserved.
   * @param  {Object Array} data Point data including x and y coordinates, name
   *                        of point, colour, and other points bundled with the
   *                        point that have the same x and y coordinates.
  */
  update_colors: (data) !->
    @_data := data
    @_points := @_points_group.selectAll \circle
      .data @_data, -> it.name

    @_points.transition!
      .style \fill  -> it.c
      .attr \cx     ~> @_x it.x
      .attr \cy     ~> @_y it.y


  /**
   * Update the point fill opacity
   * @param {Float} opacity Value for point fill opacity [0.0-1.0]
  */
  update_point_fill_opacity: (opacity) !->
    @_points.transition!
      .style \fill-opacity -> opacity


  /**
   * Update/generate the legend for the scatterplot showing the groups being
   * highlighted in the plot.
   * This function needs to be called separately from the update_data function.
   * @param  {String}       title         Legend title
   * @param  {String Array} groups        The ids of the groups being 
   *                        highlighted in the plot.
   * @param  {String Array} group_colours The HTML colours for the groups being 
   *                        highlighted in the plot.
  */
  update_legend: (title, groups, group_colours) !->
    if groups is null or group_colours is null or groups.length is 0 or group_colours.length is 0
      # remove legend if there are no groups highlighted
      @_svg.selectAll \.legend
        .transition!
        .style \fill-opacity 0
        .remove!
      @_svg.selectAll \.legend-title 
        .remove!
    else #if groups.length == group_colours.length
      # update legend with new group data and colours
      legend_data = []
      
      /*
      Check if single group specified
      */
      if typeof groups is \string
        datum =
          group: groups
          group_colour: group_colours
        legend_data.push datum
      else
        /*
        If more than one group is highlighted
        */
        for i til groups.length
          datum =
            group: groups[i]
            group_colour: group_colours[i]
          legend_data.push datum

      /*
      Remove the old legend
      */
      @_svg.selectAll \.legend .remove!

      /*
      Create new legend
      */
      legend = @_svg
        .append \g
        .attr \class \legend

      legend_background = legend.append \rect
        .attr \fill \white
        .attr \fill-opacity 0.8
      


      legend_items = legend.selectAll \.legend-item
        .data legend_data, -> it.group

      legend_items = legend_items.enter!append \g
        .attr \class \legend-item
        .attr \transform (d, i) -> 
          "translate(0,#{(i+1) * 20 + 10})"

      /*
      Create rect elements with group colours
      */
      legend_items.append \rect 
        .attr \x 0
        .attr \width 18
        .attr \height 18
        .style \fill -> it.group_colour
      
      /*
      Create text elements with group names
      */
      legend_items.append \text
        .attr \x 22
        .attr \y 9
        .attr \dy ".35em"
        .style \text-anchor \start
        .text -> it.group

      /*
      Update the legend title
      */
      legend.selectAll \.legend-title .remove!
      legend.append \text
        .attr \class \legend-title
        .attr \x 22
        .attr \y 20
        .attr \font-weight \bold
        .text title

      /*
      Get the width of the legend g element
      */
      legend_dim = legend[0][0].getBBox!
      
      legend_background
        .attr \x -5
        .attr \width legend_dim.width + 10
        .attr \height legend_dim.height + 5


      
      right_plot_extent = @width + @margin.left + @margin.right
      /*
      Translate the legend group over to the right side of the plot area
      */
      legend.attr \transform, "translate(#{right_plot_extent - legend_dim.width - 10}, 0)"


  set_metadata: (metadata) ->
    @_metadata := metadata

  /*
   * When a rotation vector line label is clicked then trigger a category data
   * change. 
  */
  click_rotation_label: (d) !->
    $ "\##{@category_select_id} option"
      .attr \value d.label
    $ "\##{@category_select_id}" .trigger \change

  # Point selection brush events

  /*
   * Start point brush selection if the shiftKey is pressed.
  */
  brush_start_event: !->
    @_points.each (d) !~>
      d.previouslySelected = @_shiftKey and d.selected
    unless @_shiftKey
      d3.event.target.clear!
      @_brush.call d3.event.target

  /*
   * While the shiftKey is pressed and mouse is clicked, continue point brush 
   * selection. Select or deselect points using a XOR boolean operation where
   * if the point is:
   * - deselected if previously selected and within the brush extent
   * - deselected if NOT previously selected and NOT within the brush extent
   * - selected if NOT previously selected and within the brush extent
   * - selected if previously selected and NOT within the brush extent
  */
  brush_event: !->
    if @_shiftKey
      extent = d3.event.target.extent!
      @_points.classed \selected (d) ~> 
        d.selected = d.previouslySelected xor (extent[0][0] <= @_x(d.x) < extent[1][0] and 
          extent[0][1] <= @_y(d.y) < extent[1][1])

    else
      d3.event.target.clear!
      @_brush.call d3.event.target

  /*
   * When brushing is complete, clear the D3 brush object, update the point
   * selection and trigger a selectionChanged event
  */
  brush_end_event: !->
    d3.event.target.clear!
    @_brush.call d3.event.target
    @set_selection!
    $ @el .trigger \selectionChanged

  /*
   * Point selection enabled with shift key press
  */
  shiftKey_press_event: !->
    @_shiftKey := d3.event.shiftKey
    if @_shiftKey
      @_rect.attr \pointer-events \none
    else
      @_rect.attr \pointer-events \all

  /*
   * Get the list of point names that are selected
  */
  get_selection: ->
    @_selection

  /*
   * Set the point selection to contain all points with the point.selection 
   * attribute set to true including overlapping/nested points under point.other
  */
  set_selection: ->
    @_selection := []
    @_points.each (d) ~>
      if d.selected
        @_selection.push d.name
        for other in d.other
          @_selection.push other.name
    @_selection

  /*
   * Clear the current point selection
  */
  clear_selection: !->
    @_points.classed \selected, (d) -> d.selected = false
    @_points.classed \newly-selected, (d) -> false
    @_selection := []
    $ @_el_id .trigger \selectionChanged

  
  /*
  Methods for panning, zooming, rescaling plot area with semantic zoom of 
  points and other elements
  */


  /*
   * Semantic zoom of the data points (points stay the same size on the screen) 
   * and zoom/rescale of X- and Y-axes
   * @param {Boolean} transition Update point (and rotation vector lines) 
   * coordinates with a smooth transition
  */
  semantic_zoom: (transition) !->
    /*
    Adjust the position of each point
    */
    p = @_points
    if transition
      p = p.transition!
    p.attr \cx ~> @_x it.x 
      .attr \cy ~> @_y it.y
    /*
    If there are rotation vector lines present then adjust their coordinates
    */
    if @_rot_lines?
      l = @_rot_lines.select \line 
      if transition
        l = l.transition!

      l.attr \x1 @_x @_x_rot 0
        .attr \y1 @_y @_y_rot 0
        .attr \x2 ~> @_x @_x_rot it.x
        .attr \y2 ~> @_y @_y_rot it.y
      t = @_rot_lines.select \text
      if transition
        t = t.transition!
      t.attr \x ~> @_x @_x_rot it.x
        .attr \y ~> 
          y_offset = if it.y < 0 then 10 else 0
          y_offset + @_y @_y_rot it.y
    /*
    Readjust the X- and Y-axes
    */
    @_svg.select ".x.axis" .call @_xAxis
    @_svg.select ".y.axis" .call @_yAxis

  /*
   * Update the X and Y scales of the Scatterplot plot area including axes 
   * scales and zooming behaviour of the plot 
  */
  rescale_plot_area: !->
    /*
    Readjust the X- and Y-axes
    */
    @_svg.select ".x.axis" .call @_xAxis
    @_svg.select ".y.axis" .call @_yAxis
    /*
    (re)initialize the semantic zoom behaviour
    */
    zoom_behavior = d3.behavior.zoom!.x @_x .y @_y .on \zoom !~>
      @semantic_zoom false
    /*
    enable the semantic zoom behaviour
    */
    @_points_group.call zoom_behavior

  /*
   * Reset the view on the plot so that all points are nicely viewable and zoom
   * and rescale the X- and Y-axes
  */
  reset_view: !->
    /*
    determine the new x and y extents
    */
    @_x.domain(d3.extent @_data,  -> it.x).nice!
    @_y.domain(d3.extent @_data,  -> it.y).nice!

    @rescale_plot_area!

    @semantic_zoom true

  /*
   * Generate HTML string for table headers given a list of header names
   * @param {String Array} headers List of header names
   * @return {String} HTML string for table headers
  */
  get_table_headers_html: (headers) ->
    return ["<th>#{header}</th>" for header in headers].join('')

  /*
   * Generate the tbody HTML contents for the point info table for each metadata
   * attribute that the user wants to display in the hover over table for the 
   * currently selected scatterplot point.
   * @param {String Array} attrs List of attributes to show in the metadata
   * table point info table
   * @param {Object} point D3 Scatterplot point data
   * @param {String} point.name Name of point
   * @param {Object Array} point.other Scatterplot point data for points at same
   * x and y coordinates. 
   * @return {String} HTML string for table tbody contents
  */
  get_table_body_html: (attrs, point) ->
    point_names = [point.name] ++ [p.name for p in point.other]

    html = ''
    for point_name in point_names
      point_metadata = @_metadata[point_name]
      vals = ["<td>#{point_metadata[attr]}</td>" for attr in attrs]
      html += "<tr>#{vals.join('')}</tr>"

    return html

  /*
   * Generate the HTML for the contents of the point information table to be 
   * shown in the point tooltip.
   * @param  {Object} d Point data and info
   * @return {String}   HTML string containing the point info
  */
  get_table_html: (d) ->
    return unless @_metadata?
    return unless d.name of @_metadata
    point_metadata = @_metadata[d.name]

    attrs = [k for k of point_metadata]

    headers_html = @get_table_headers_html attrs

    tbody_html = @get_table_body_html attrs, d

    html = """
      <thead>
        <tr>
          #{headers_html}
        </tr>
      </thead>
      <tbody>
        #{tbody_html}
      </tbody>
      """

    return html

  
  /**
   * Show the point information tooltip containing a table showing the point 
   * information. 
   *
   * DataTables.js is required for formatting the tooltip table as a DataTables
   * table with filtering, sorting and searching enabled
   * 
   * @param  {Object} d Point data and info
  */
  show_tooltip: (d) !->
    return unless @_metadata?
    return unless d.name of @_metadata

    unless @_tt_dragged
      x_pos = _mouse_pos.x + 10
      y_pos = _mouse_pos.y + 10
      @_tt.style \left "#{x_pos}px"
        .style \top "#{y_pos}px"
    
    @_tt.select \.tooltip_table_container .remove!
    table = @_tt.append \div 
      .attr \class \tooltip_table_container
      .append \table
      .attr \class "table display"
      .attr \id \tooltip_table
      .attr \width \100%
    table.html @get_table_html d
    if @_show_tt
      @_tt.style \border "2px solid green"
    else
      @_tt.style \border "2px solid red"
    @_tt.style \display \block

    $ "\##{@_el_id} \#tooltip_table" .dataTable do
      \bPaginate : false
      \bLengthChange : false
      \bFilter : true
      \bSort : true
      \bInfo : false
      \bAutoWidth : true

  /**
   * Hide the point information tooltip
  */
  hide_tooltip: !->
    @_tt_dragged := false
    @_show_tt := false
    @_tt.style \display \none
  
