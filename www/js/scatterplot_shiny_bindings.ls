/*
Shiny input/output bindings and custom message handlers for custom D3
scatterplot (scatterplot.ls).

Requires: 
  - scatterplot.ls/js
  - D3.js

*/


/**
 * Dictionary of scatterplot ids to Scatterplot objects
 * @type {Object}
*/
scatterplots = {}
/*
FOR DEBUGGING:
Add scatterplots dict to global object for debugging purposes
*/
# window.scatterplots = scatterplots


init_scatterplot = (el, data) ->
  scatterplot = new Scatterplot el

  if data.reset_view_id?
    $ "\##{data.reset_view_id}" 
      .on \click ->
        scatterplot.reset_view!

  return scatterplot


/*
 * Get Scatterplot object corresponding to an HTML div id
 * @param {String} data.id D3 Scatterplot object HMTL id
 * @return {Scatterplot} If data.id exists within scatterplots dictionary and 
 * corresponds to a Scatterplot object
*/
get_scatterplot = (data) ->
  unless data?
    return

  unless data.id?
    return

  unless data.id of scatterplots
    return
  scatterplot = scatterplots[data.id]
  return scatterplot

objectify_scatterplot_data = (data) ->
  scatterplot_data = {}
  i = 0

  for i til data.names.length
    key = data.xs[i] + "," + data.ys[i]
    tmp =
      index: i
      x: data.xs[i]
      y: data.ys[i]
      name: data.names[i]
      c: (unless data.colours? then "black" else data.colours[i])
      selected: (unless data.selected? then false else data.selected[i])
      other: []

    if key of scatterplot_data
      /*
      If the x,y coordinate key already exists append it to the list of "other"
      points at those coordinates
      */
      scatterplot_data[key]["other"].push tmp
    else
      scatterplot_data[key] = tmp

  return d3.values scatterplot_data

/*
 * Take a 2D array of scatterplot point data and array of point names and return
 * an object/dict with point names -> point attributes
 * @param {String Array} data.names Point names/keys
 * @param {Object} data.md Point attributes and array of values from metadata 
 * dataframe
*/
objectify_metadata = (data) ->
  md = {}
  for name, i in data.names
    tmp = {}
    for k, v of data.md
      val = v[i]
      if val?
        tmp[k] = v[i]
    md[name] = tmp
  return md

/*
Shiny output binding for initializing and plotting a Scatterplot object
*/
scatterplots_output = new Shiny.OutputBinding!
$.extend scatterplots_output,
  find: (scope) ->
    $(scope).find \.scatterplot
  onValueError: (el, err) !->
  renderValue: (el, data) !->
    /*
    Check that the necessary data exists for creating or updating the 
    Scatterplot
    */
    return unless data?
    return unless data.xs?
    return unless data.ys?
    return unless data.names?
    
    /*
    Get the Scatterplot object if it exists or initialize a new Scatterplot 
    object for a particular Scatterplot container div id
    */
    if el.id of scatterplots
      scatterplot = scatterplots[el.id]
    else
      scatterplot = init_scatterplot el, data
      /*
      Add newly initialized Scatterplot to scatterplots dict
      */
      scatterplots[el.id] = scatterplot

    /* 
    Objectify data from R so that it can be used effectively with D3
    */
    scatterplot_data = objectify_scatterplot_data data

    /*
    If the current Scatterplot object doesn't have data then update the new data
    */
    unless scatterplot._data?
      scatterplot.update_data scatterplot_data, data.x_label, data.y_label
      scatterplot.xs := data.xs
      scatterplot.ys := data.ys
      scatterplot.reset_view!

    /*
    If new data has the same x and y coordinates, then change the point colours
    */
    if data.colours? and scatterplot.xs === data.xs and scatterplot.ys === data.ys
      scatterplot.update_colors scatterplot_data
    else
      /*
      If the x,y coordinates are different then update the point coordinates,
      rescale the plot and reset the plot view
      */
      scatterplot.update_data scatterplot_data, data.x_label, data.y_label
      scatterplot.xs := data.xs
      scatterplot.ys := data.ys
      scatterplot.rescale_plot_area!
      scatterplot.reset_view!

    /*
    Update the legend title, groups and group colours
    */
    if data.selected_group? and data.groups? and data.group_colours?
      scatterplot.update_legend data.selected_group, data.groups, data.group_colours
    
    if data.md?

      metadata = objectify_metadata data
      scatterplot.set_metadata metadata
    
Shiny.outputBindings.register scatterplots_output


/*
Return Scatterplot selection back to R Shiny using InputBinding
*/
scatterplots_selection_input = new Shiny.InputBinding!
$.extend scatterplots_selection_input,
  find: (scope) ->
    $(scope).find \.scatterplot
  getValue: (el) ->
    scatterplot_id = el.id#.replace /_selection/, ''

    /*
    Get the Scatterplot object corresponding to the selection Shiny input div
    */
    if scatterplot_id of scatterplots
      scatterplot = scatterplots[scatterplot_id]
    else
      return []

    selection_panel = $ "\##{el.id}_selection_panel"
    selection_panel.on \dialogbeforeclose, (event, ui) ->
      scatterplot.clear_selection!
    genome_sel = scatterplot.get_selection!
    if genome_sel.length > 0
      $ "\##{el.id}_clear_selection"
        .on \click !-> 
          scatterplot.clear_selection!
          selection_panel.dialog \close

      selection_panel.dialog \option, \position do 
        my: 'left top'
        at: 'right top'
        of: $ "\##{el.id} svg"

      selection_panel.dialog \open
    else
      selection_panel.dialog \close
    return genome_sel
    
  subscribe: (el, callback) ->
    $(el).on \selectionChanged, (e) !-> 
      callback!

Shiny.inputBindings.register scatterplots_selection_input

/**
 * Update D3 PCA scatterplot rotation vector lines including updating labels,
 * coordinates and hiding everything. 
 * Rotation vector lines are specific to principal component analysis (PCA)
 * 
 * @param {Object}  data      Object with info for updating rotation vector lines
 * @param {String}  data.id   D3 scatterplot HTML element id 
 * @param {Boolean} data.hide Hide the rotation vector lines?
 * @param {String Array} data.rot_labels Rotation labels
 * @param {Float Array}  data.rotx       Rotation vector x coordinates
 * @param {Float Array}  data.roty       Rotation vector y coordinates
*/
Shiny.addCustomMessageHandler \update_scatterplot_rotation_vectors  (data) !->
  scatterplot = get_scatterplot data
  unless scatterplot?
    return
  if data.hide
    scatterplot.remove_rotation_vectors!
    return
  select_id = if data.category_select_id? then data.category_select_id else null
  scatterplot.update_rotation_vectors data.rot_labels, data.rotx, data.roty, select_id

/**
 * Update the Scatterplot point fill opacity using data from R
 * @param {String} data.id D3 Scatterplot object HTML id
 * @param {Float} data.opacity Opacity value between 0.0-1.0
*/
Shiny.addCustomMessageHandler \update_scatterplot_point_opacity (data) !->
  scatterplot = get_scatterplot data
  unless scatterplot?
    return

  scatterplot.update_point_fill_opacity data.opacity

/*
 * Update the scatterplot point metadata which is displayed in a hover over 
 * table in a draggable dialog. 
 * @param {String} data.id D3 Scatterplot object HTML id
 * @param {String Array} data.names Point keys/names
 * @param {Object} data.md Point attributes and array of values from metadata 
 * dataframe
*/
Shiny.addCustomMessageHandler \update_scatterplot_metadata (data) !->
  scatterplot = get_scatterplot data
  unless scatterplot?
    return

  unless data.names?
    return
  unless data.md?
    return
  metadata = objectify_metadata data
  scatterplot.set_metadata metadata
