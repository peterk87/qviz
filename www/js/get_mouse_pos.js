_mouse_pos = {x:0, y:0};

/*
 * Handle mouse move events to keep track of the current mouse position.
 *
 * This function sets the current mouse x and y positions in _mouse_pos.
 * 
 * @param  {Object} event Mousemove event object
*/
handle_mousemove_event = function(event){
  event = event || window.event;
  _mouse_pos = {
    x: event.clientX,
    y: event.clientY
  };
};

$(window).on("mousemove", handle_mousemove_event);