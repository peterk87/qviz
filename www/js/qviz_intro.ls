/**
 * Start the Intro.js demo/intro for the Qviz app. 
 * @param {string} which  Select which Intro.js steps to show. Default is "all" 
 *                        which shows the intro for the following: genome_stats,
 *                        genome_upload, basic_plots, pca
 */
export start_intro: (steps="all") !->
  console.log 'start_intro', steps
  get_intro_steps = (steps) ->
    steps_dict = do
      genome_stats: 
        * element: \#load_default_data
          intro: """
            <p>
              Pressing this button loads the demo dataset.
            </p>
            <p>
              The demo dataset will now be loaded to demonstrate the capabilities of Qviz.
            </p>
            """
          position: \right
        * element: \#view_report_table
          intro: "Assembly statistics for the demo dataset are shown in this data table."
          position: \left
        * element: \#ul_quast_stats
          intro: "You can upload a previously generated Qviz genome statistics and continue an analysis by adding more genomes, creating user-defined groups, etc."
          position: \right
        * element: \#dl_quast_stats
          intro: """
            <p>
              Download the Qviz generated genome quality statistics as a CSV to continue with your own analyses.
            </p>
            <p>
              Any extra fields you add to the CSV file outside of Qviz can be visualized within Qviz. You just need to upload the modified CSV file to Qviz.
            </p>
            """
          position: \right
      genome_upload:
        * element: document.querySelector \.nav .children[2] 
          intro: "You can upload genomes for Qviz analysis in this tab."
          position: \bottom
        * element: \#ul_genome
          intro: """
            <p>
              Select genome files in <b>FASTA</b> format to upload and analyze using Qviz.
            </p>
            <p>
              You can select <b>multiple</b> files for upload.
            </p>
            <p>
              <em>Note: Analysis will proceed with valid FASTA format files only.</em>
            </p>
            """
          position: \right
      single_var_plots:
        * element: document.querySelector \#tabs .children[1] 
          intro: "Visualize genome statistics like the N50, number of contigs and number of genes with scatterplots or bar plots."
          position: \bottom
      basic_plots:
        * element: document.querySelector \#tabs .children[4] 
          intro: "Visualize genome statistics with bar plots, scatterplots and other basic plots in this tab."
          position: \bottom
        * element: \#ui_select_strain
          intro: "Select genomes to plot"
          position: \right
        * element: \#ui_select_variables
          intro: "Select genome statistics to plot"
          position: \right
        * element: \#ui_order_strains_by
          intro: "Select genome statistic to order genomes by"
          position: \right
        * element: \#ui_colour_category
          intro: "Select category to highlight in plot. The default category to highlight in the plot is the assembly."
          position: \right
        * element: \#ui_colour_group
          intro: """
            <p>
              Select one or more assemblies to highlight in the plot.
            </p>
            <p>
              If other categories are available, you can highlight one or more groups of genomes of the specified category.
            </p>
            """
          position: \right
        * element: \#plot_type
          intro: "Select the plot type."
          position: \right
        * element: \#log_scale
          intro: "Should the values be log scale to better visualize large differences in values?"
          position: \right
        * element: \#update_basic_plot
          intro: "When you have confirmed your selections, you can press this button to update the plot."
          position: \right
        * element: \#ngs_ggplot
          intro: "This is a plot of the genomes and statistics you have selected in the sidebar panel."
          position: \left
      scatterplots: 
        * element: document.querySelector \#tabs .children[5]
          intro: "View one assembly statistic versus another in an interactive scatterplot."
          position: \bottom
        * element: \#scatterplots
          intro: """
            <p>
              This is the interactive scatterplot of one assembly statistic versus another. You can interact with this plot in the following ways:
            </p>
            <ul>
              <li>
                <b>Pan</b> by clicking and dragging
              </li>
              <li>
                <b>Zoom</b> by scrolling the mouse wheel
              </li>
              <li>
                <b>Click</b> on a data point to show a tooltip table for the selected genome
              </li>
              <li>
                <b>Hold Shift</b> to select genomes to show genome statistics for the selected genomes and to create user defined groups
              </li> 
            </ul>
            """
          position: \left
        * element: \#scatterplot_reset_view
          intro: "scatterplot_reset_view"
          position: \right
        * element: \#x_variable+div
          intro: "x_variable"
          position: \right
        * element: \#y_variable+div
          intro: "y_variable"
          position: \right
        * element: \#ui_scatterplot_highlight_category
          intro: "ui_scatterplot_highlight_category"
          position: \right
        * element: \#scatterplot_log_scale
          intro: "scatterplot_log_scale"
          position: \right
        * element: \#scatterplots_opacity_input+span
          intro: "scatterplots_opacity_input"
          position: \right
      pca:
        * element: document.querySelector \#tabs .children[6]
          intro: "Perform Principal Component Analysis (PCA) of genome quality statistics to visualize similarities and differences between genome assemblies."
          position: \bottom 
        * element: \#jspca
          intro: """
            <p>
              This is the PCA plot. You can interact with this plot in the following ways:
            </p>
            <ul>
              <li>
                <b>Pan</b> by clicking and dragging
              </li>
              <li>
                <b>Zoom</b> by scrolling the mouse wheel
              </li>
              <li>
                <b>Click</b> on a data point to show a tooltip table for the selected genome
              </li>
              <li>
                <b>Click</b> on a genome quality statistic label to highlight that statistic on the genome data points
              </li>
              <li>
                <b>Hold Shift</b> to select genomes to show genome statistics for the selected genomes and to create user defined groups
              </li> 
            </ul>
            """
          position: \left
        * element: \#pca_reset_view
          intro: "Reset the plot view to nicely fit all points in the plot area"
          position: \right
        * element: \#pca_categories+div
          intro: "Select genome quality statistics to perform PCA with"
          position: \right
        * element: \#pca_scale
          intro: "Scale genome quality statistics for PCA computation?"
          position: \right
        * element: \#pca_show_vectors
          intro: "Show genome statistic vectors on plot?"
          position: \right
        * element: \#ui_pca_x
          intro: "Select X-axis Principal Component (PC)"
          position: \right
        * element: \#ui_pca_y
          intro: "Select Y-axis Principal Component (PC)"
          position: \right
        * element: \#ui_d3pca_category_selection
          intro: "Select the statistic or category to highlight in the plot"
          position: \right
        * element: \#pca_log_scale_colour
          intro: "Log10 scaling of PCA point colour highlighting?"
          position: \right
        * element: \#pca_fill_opacity+span
          intro: "Change the opacity of the PCA points"
          position: \right

    if steps == \all
      return steps_dict.genome_stats ++ steps_dict.genome_upload ++ steps_dict.single_var_plots ++ steps_dict.basic_plots ++ steps_dict.scatterplots ++ steps_dict.pca 
    else if steps of steps_dict
      steps_dict[steps]
    else
      null

  intro = introJs!

  intro_steps = get_intro_steps steps

  func_dict = 
    \load_default_data : !-> 
      $ "\.nav li a:contains(\"App\")" .trigger \click
      $ "\#tabs li a:contains(\"Genome Stats\")" .trigger \click
      $ \#load_default_data .trigger \click
    \dl_quast_stats : !->
      $ "\.nav li a:contains(\"App\")" .trigger \click
      $ "\#tabs li a:contains(\"Genome Stats\")" .trigger \click
    "Upload Genomes": !->
      $ "\.nav li a:contains(\"Upload Genomes\")" .trigger \click
    \ul_genome : !->
      console.log "ul_genome highlighted"
    "N50 Plot": !->
      $ "\.nav li a:contains(\"App\")" .trigger \click
      $ "\#tabs li a:contains(\"N50 Plot\")" .trigger \click
    "Basic Plots": !->
      $ "\.nav li a:contains(\"App\")" .trigger \click
      $ "\#tabs li a:contains(\"Basic Plots\")" .trigger \click
    \update_basic_plot : !->
      $ \#update_basic_plot .trigger \click
    "PCA": !->
      $ "\.nav li a:contains(\"App\")" .trigger \click
      $ "\#tabs li a:contains(\"PCA\")" .trigger \click
    "Scatterplots": !->
      $ "\.nav li a:contains(\"App\")" .trigger \click
      $ "\#tabs li a:contains(\"Scatterplots\")" .trigger \click

  intro.setOptions do
    steps: intro_steps.filter (o) -> $ o.element .length

  intro
    .onchange (el) !->
      if el.id of func_dict
        func_dict[el.id]!
      else if el.children.length > 0 
        if el.children[0].text of func_dict
          func_dict[el.children[0].text]!
    .start!

