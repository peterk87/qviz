
quast_metric_desc =
  Assembly: 'Name of genome assembly'
  contigs_gt_0bp: 'The total number of bases in contigs of length ≥ 0, 500, 1000 bp.'
  contigs_gt_500bp: 'The total number of bases in contigs of length ≥ 0, 500, 1000 bp.'
  contigs_gt_1000bp: 'The total number of bases in contigs of length ≥ 0, 500, 1000 bp.'
  Largest_contig: 'The length of the longest contig in the assembly.'
  length_0_bp: 'The total number of bases in contigs of length ≥ 0, 500, 1000 bp.'
  length_gt_500bp: 'The total number of bases in contigs of length ≥ 0, 500, 1000 bp.'
  length_1000_bp: 'The total number of bases in contigs of length ≥ 0, 500, 1000 bp.'
  GC: 'GC (%) is the total number of G and C nucleotides in the assembly, divided by the total length of the assembly.'
  N50: 'N50 is the length for which the collection of all contigs of that length or longer covers at least half an assembly.'
  N75: 'N75 is the length for which the collection of all contigs of that length or longer covers at least 75% an assembly.'
  L50: 'L50 is the number of contigs as long as N50; the minimal number of contigs that cover half the assembly.'
  L75: 'L50 is the number of contigs as long as N50; the minimal number of contigs that cover half the assembly.'
  Ns_per_100_kbp: 'The number of N\'s per 100 kbp is the average number of uncalled bases (N\'s) per 100000 assembly bases.'
  n_genes_unique: 'The number of unique predicted genes in the assembly found by GeneMark'
  genes_gt_0bp: 'The number of predicted genes ≥ 0bp in the assembly found by GeneMark'
  genes_gt_300bp: 'The number of predicted genes ≥ 300bp in the assembly found by GeneMark'
  genes_gt_1500bp: 'The number of predicted genes ≥ 1500bp in the assembly found by GeneMark'
  genes_gt_3000bp: 'The number of predicted genes ≥ 3000bp in the assembly found by GeneMark'


set_table_header_title = (value, title) !->
  $ "th:contains('#value')" .attr \title title

set_table_input_title = (value, title) !->
  $ "th>input[placeholder='#value']" .attr \title title

set_selectize_title = (value, title) !->
  $ "div.item[data-value=\"#{value}\"]" .attr \title title

Shiny.addCustomMessageHandler \update_assembly_stat_tooltips  (data) !->
  if data? and data.disable_tooltips? 
    $ document .tooltip disabled: data.disable_tooltips
      

  for value of quast_metric_desc
    title = quast_metric_desc[value]
    set_selectize_title value, title

  set_table_tooltips = !->
    for value of quast_metric_desc
      title = quast_metric_desc[value]
      set_table_header_title value, title
      set_table_input_title value, title

  setTimeout set_table_tooltips, 5000

  $ document .tooltip!
